const express = require('express');
const cors = require('cors');
const { newTodo, updateTodo } = require("./types")
const { todo } = require("./db");
const bodyParser = require('body-parser');
const app = express();

app.use(express.json());
app.use(cors({
    origin: "http://localhost:3002"
}));
app.use(bodyParser.json());

app.post('/todo', async (req, res) => {
    // input validation
    const userInput = req.body
    try {
        const parsedInput = newTodo.safeParse(userInput)
        if (parsedInput.success) {
            await todo.create({
                title: userInput.title,
                description: userInput.description,
                completed: false
            })
            res.status(201).json({message: "Your Todo is created"});
        } else {
            res.status(411).json({message: "Please send correct inputs"});
        } 
    } catch(err) {
        res.status(500).json({message: "Oops!!, we have some internal error"});
    }
})


app.get('/todos', async (req, res) => {
    try {
        const allTodos = await todo.find({})
        res.status(200).json(allTodos)
    } catch(err) {
        res.status(500).json({message: "Oops!!, some error occured"});
    }
})

app.put('/completed', async (req, res) => {
    const userInput = req.body
    try {
        const parsedInput = updateTodo.safeParse(userInput)
        if (parsedInput.success) {
            await todo.updateOne({ _id: userInput._id}, { $set: {completed: true}})
            res.status(200).json({message: "Your Todo is marked"});
        } else {
            res.status(411).json({message: "Please send correct inputs"})
            return;
        }
    } catch(err) {
        res.status(500).json({message: "Oops!!, we could not mark the todo"});
    }
})

app.listen(4000, () => {
    console.log("Server is running on port 4000");
})